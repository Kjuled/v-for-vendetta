capital = 615

oob = "ITA_1936"

set_research_slots = 3
set_stability = 0.25
set_war_support = 0.05

set_politics = {
	ruling_party = communism
	last_election = "1994.3.26"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	democratic = 0
	fascism = 0
	communism = 100
	neutrality = 0
}

create_country_leader = {
	name = "Benito Mussolini"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "Portrait_Italy_Benito_Mussolini.dds"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {
		
	}
}

create_country_leader = {
	name = "Tao Siju"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "GFX_portrait_italy_ferruccio_parri"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {
		
	}
}

create_country_leader = {
	name = "Anatoly Sobchak"
	desc = "POLITICS_VICTOR_EMMANUEL_III_DESC"
	picture = "portrait_REG_anatoly_sobchak.dds"
	expire = "1965.1.1"
	ideology = oligarchism
	traits = {

	}
}