
state={
	id=198
	name="STATE_198"
	manpower = 11933

	state_category = depopulated_area

	history={
		victory_points = {
			476 1
		}
		owner = UKR
		buildings = {
			infrastructure = 0
			air_base = 0
		}
		add_core_of = SOV
		add_core_of = UKR
	}

	provinces={
		476 6455 6480 9423 9435 9481 9576 
	}
}
