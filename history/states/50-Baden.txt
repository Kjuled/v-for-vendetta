state={
	id=50
	name="STATE_50"
	resources={
		aluminium=6 # was: 10.000
	}

	state_category = depopulated_area
	history={
		owner = WGR
		victory_points = {
			9517 1
		}
		victory_points = {
			11499 1 #10
		}
		#victory_points = {
		#	3530 10
		#}
		#victory_points = {
		#	11640 1
		#}
		#victory_points = {
		#	6712 3
		#}
		buildings = {
			infrastructure = 0
			arms_factory = 0
			industrial_complex = 0
			air_base = 0

		}
		add_core_of = WGR
	}

	provinces={
		519 694 3530 3679 3690 3692 6542 6555 6568 6581 6712 6934 9517 9545 9655 11486 11499 11640 13126 
	}
	manpower=82351
	buildings_max_level_factor=1.000
}
