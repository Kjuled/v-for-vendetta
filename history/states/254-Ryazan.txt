
state={
	id=254
	name="STATE_254"
	manpower = 24120
	
	state_category = depopulated_area

	history={
		owner = MEZ
		set_demilitarized_zone = yes
		buildings = {
			infrastructure = 0
		}
		add_core_of = MEZ
		victory_points = {
			11248 1
		}
	}

	provinces={
		269 283 314 330 397 3257 3275 3302 3322 3356 3387 6279 6284 9266 9273 9313 9330 9376 11248 11258 11294 11314 11363 11379 
	}
}
