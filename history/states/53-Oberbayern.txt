
state={
	id=53
	name="STATE_53"
	manpower = 253365
	
	state_category = depopulated_area

	history={
		owner = AUS
		buildings = {
			infrastructure = 1
		}
		add_core_of = AUS
		victory_points = {
			9515 2
		}
	}

	provinces={
		532 571 586 3299 3541 3571 6725 9515 9681 11497 
	}
}
