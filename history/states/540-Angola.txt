
state={
	id=540
	name="STATE_540"
	resources={
		rubber=2
	}

	history={
		owner = UNT
		buildings = {
			infrastructure = 1
		}
		victory_points = {
			8248 1 
		}
		add_core_of = UNT
	}

	provinces={
		4735 8202 12391 12985 
	}
	manpower=1200000
	buildings_max_level_factor=1.000
	state_category=rural
}
