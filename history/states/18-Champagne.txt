
state={
	id=18
	name="STATE_18"
	manpower = 801
	
	state_category = formerly_settled_area 

	history={
		owner = FRA
		victory_points = {
			9490 1 
		}
		victory_points = {
			13011 1
		}
		victory_points = {
			3560 1
		}
		buildings = {
			infrastructure = 0
			industrial_complex = 0
			air_base = 0
		}
		add_core_of = FRA
	}

	provinces={
		551 3533 3546 3560 5291 6531 6545 9472 9490 9505 11732 13011 
	}
}
