
state={
	id=43
	name="STATE_43" # Northern Hungary
	manpower = 12752
	resources={
		steel=4 # was: 4
		aluminium=24 # was: 194
	}
	
	state_category = depopulated_area

	history={
		owner = HUN
		buildings = {
			infrastructure = 0
			arms_factory = 0
			industrial_complex = 0
			air_base = 0
		}
		victory_points = {
			9660 20 
		}
		add_core_of = HUN
	}

	provinces={
		684 716 3713 3731 6716 6751 9660 9690 11520 
	}
}
