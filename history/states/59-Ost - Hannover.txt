
state={
	id=59
	name="STATE_59"
	manpower = 38286
	
	state_category = depopulated_area

	history={
		owner = WGR
		victory_points = {
			3326 1
		}
		victory_points = {
			6377 1
		}
		buildings = {
			infrastructure = 0
			arms_factory = 0
			industrial_complex = 0
			dockyard = 0

		}
		add_core_of = WGR
	}

	provinces={
		374 526 3271 3326 3395 6218 6263 6298 6325 6349 6377 6513 9238 9264 9375 11402 11468 11493 
	}
}
