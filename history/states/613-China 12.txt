
state={
	id=613
	name="STATE_613" # Shanghai
	manpower = 9510
	
	state_category = depopulated_area
	
	history={
		owner = CHI
		add_core_of = CHI
		add_core_of = PRC
		buildings = {
			infrastructure = 0
			industrial_complex = 0
			air_base = 0
			dockyard = 0
		}
		victory_points = {
			11913 1
		}
		victory_points = {
			7014 1
		}
		victory_points = {
			10076 1
		}
	}

	provinces={
		996 1089 4091 7014 7120 10034 10076 11913 11928 11982 12052 12076 13142 
	}
}
