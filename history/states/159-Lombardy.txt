
state={
	id=159
	name="STATE_159"
	manpower = 98704
	state_category = depopulated_area

	history={
		owner = PAD
		victory_points = {
			3780 1 
		}
		buildings = {
			infrastructure = 0
			arms_factory = 0
			industrial_complex = 0
			air_base = 0

		}
		add_core_of = PAD
		1939.1.1 = {
			buildings = {
				arms_factory = 7
				industrial_complex = 4
			}
		}
	}

	provinces={
		607 773 969 3776 3780 6661 9584 11568 11587 11734 
	}
}
