state={
	id=796
	name="STATE_796"
	resources={
		oil=8
	}
	history={
		owner = UNT
		buildings = {
			infrastructure = 1
			2115 = {
				naval_base = 1
			}
		}
		victory_points = {
			2115 1 
		}
		add_core_of = UNT
	}
	provinces={
		2115 
	}
	manpower=129000
	buildings_max_level_factor=1.000
	state_category=rural
}
