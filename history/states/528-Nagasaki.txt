
state={
	id=528
	name="STATE_528" # Kyushu
	manpower = 69000
	state_category = depopulated_area
	resources={
		chromium=8 # was: 88
	}

	history={
		owner = JAP
		add_core_of = JAP
		buildings = {
			infrastructure = 1

		}
		victory_points = {
			1025 1
		}
		victory_points = {
			4102 1
		}
		victory_points = {
			9950 1
		}

	}

	provinces={
		1020 1025 1158 1183 4027 4102 4164 4198 7110 7178 9950 10011 10020 10092 11925 11949 12006 12032 
	}
}
