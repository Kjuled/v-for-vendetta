
state={
	id=206
	name="STATE_206"
	manpower =  13163
	
	state_category = depopulated_area

	history={
		owner = BLR
		victory_points = {
			11370 20
		}
		victory_points = {
			9289 5
		}
		buildings = {
			infrastructure = 0
			industrial_complex = 0
			air_base = 0
		}
		add_core_of = SOV
		add_core_of = BLR
	}

	provinces={
		216 294 342 3267 3378 6292 9289 11216 11322 11370 
	}
}
