capital = 214

oob = "ITA_1936"

set_research_slots = 3
set_stability = 0.25
set_war_support = 0.05

set_politics = {
	ruling_party = neutrality
	last_election = "1994.3.26"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	democratic = 14
	fascism = 6
	communism = 28
	neutrality = 52
}

create_country_leader = {
	name = "Benito Mussolini"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "Portrait_Italy_Benito_Mussolini.dds"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {
		
	}
}

create_country_leader = {
	name = "Ferruccio Parri"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "GFX_portrait_italy_ferruccio_parri"
	expire = "1965.1.1"
	ideology = socialism
	traits = {
		
	}
}

create_country_leader = {
	name = "Anatoly Sobchak"
	desc = "POLITICS_VICTOR_EMMANUEL_III_DESC"
	picture = "portrait_REG_anatoly_sobchak.dds"
	expire = "1965.1.1"
	ideology = oligarchism
	traits = {

	}
}

create_country_leader = {
	name = "Herr Polen"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "GFX_portrait_ita_palmiro_togliatti"
	expire = "1965.1.1"
	ideology = marxism
	traits = {
		
	}
}