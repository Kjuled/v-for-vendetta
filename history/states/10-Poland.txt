state={
	id=10
	name="STATE_10"
	manpower = 19550

	state_category = depopulated_area

	history={
		owner = PPR
		victory_points = {
			3544 1
		}
		victory_points = {
			402 1
		}
		buildings = {
			infrastructure = 0
		}
		add_core_of = PPR
		add_core_of = POL
	}

	provinces={
		402 467 524 3482 3544 6511 9400 11492 12562 
	}
}
