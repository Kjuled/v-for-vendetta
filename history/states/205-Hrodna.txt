
state={
	id=205
	name="STATE_205" # Kaluga
	manpower = 11214
	
	state_category = depopulated_area

	history={
		owner = MEZ
		set_demilitarized_zone = yes
		buildings = {
			infrastructure = 0
		}
		add_core_of = MEZ
		victory_points = {
			382 1
		}
	}

	provinces={
		246 274 352 382 3263 3308 3345 3377 6252 6365 9261 9301 11235 11284 11300 11369 
	}
}
