state= {
	id=37
	name="STATE_37"
	manpower = 14114

	state_category = depopulated_area

	history=
	{
		owner = SWE
		victory_points = { 
			6287 20 
		}
		victory_points = {
			3325 5
		}
		buildings = {
			infrastructure = 0
			6361 = {
				naval_base = 1
			}
		}
		add_core_of = SWE
	}

	provinces= {
		3260 3305 3325 3374 6287 6361 6393 
	}
}
