
state={
	id=608
	name="STATE_608"
	manpower = 401
	
	state_category = formerly_settled_area

	history={
		owner = BEI
		add_core_of = BEI
		buildings = {
			infrastructure = 0
			industrial_complex = 0
			air_base = 0
		}
		victory_points = {
			9843 1
		}
		victory_points = {
			10068 1 
		}
	}

	provinces={
		1134 4137 4140 6828 9843 9969 10068 11761 12026 
	}
}
