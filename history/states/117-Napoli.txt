
state={
	id=117
	name="STATE_117" # Campania
	manpower = 63611
	
	state_category = depopulated_area
	resources={
		aluminium = 4 # was: 16
		steel = 4 # was: 24
	}

	history={
		owner = ITA
		victory_points = {
			819 1
		}
		buildings = {
			infrastructure = 0
			arms_factory = 0
			industrial_complex = 0
			air_base = 0

		}
		add_core_of = ITA
	}

	provinces={
		819 851 883 955 3958 3968 6843 6939 6972 6980 9826 11803 
	}
}
